package com.company;

import javax.sound.midi.SysexMessage;

public class Main {

    public static void main(String[] args) {
        User[] users = new User[3];

        users[0] = new User("1user", 5000);
        users[1] = new User("2user", 4000);
        users[2] = new User("3user", 6000);

        for(User user : users) {
            user.setId();
            System.out.println("id: " + user.getId()
                    +  "\nName: " + user.getName()
                    + "\nMoney: " + user.getMoney()
                    + "\nNextAvailableID: "+ User.getNextId()
                    + "\n");
        }
        System.out.println("Before money rise");
        System.out.println(users[1].getName() + " " + users[1].getMoney());
        users[1].riseMoney(10);
        System.out.println("After money rise");
        System.out.println(users[1].getName() + " " + users[1].getMoney());
    }
}
