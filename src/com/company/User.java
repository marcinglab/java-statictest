package com.company;

public class User {
    private static int nextId = 1;

    private String name;
    private double money;
    private int id;

    User(String s, double m) {
        name = s;
        money = m;
        id = 0;
    }

    public static int getNextId() {
        return nextId;
    }

    public static void setNextId(int nextId) {
        User.nextId = nextId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public void setId() {
        id = nextId;
        nextId++;
    }

    public void riseMoney(int up) {
        double rise = money * up / 100;
        money += rise;
    }

    //Unit test
    public static void main(String[] args) {
        User user = new User("Test", 4000.50);
        System.out.println("name: " + user.getName() + " money: " + user.getMoney());
    }
}
